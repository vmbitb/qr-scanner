import { Component } from '@angular/core';
import { Registre } from 'src/app/models/registre.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  constructor(public data: DataService) {}

  enviarEmail() {
    this.data.enviarEmail();
  }

  obrirRegistre(registre: Registre) {
    this.data.obrirRegistre(registre);
  }
}
