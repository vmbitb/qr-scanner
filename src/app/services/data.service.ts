import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { File } from '@ionic-native/file/ngx';
import { Registre } from 'src/app/models/registre.model';
import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NavController } from '@ionic/angular';

import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  registres: Registre[] = [];

  constructor(
    private storage: Storage,
    private navController: NavController,
    private inAppBrowser: InAppBrowser,
    private file: File,
    private emailComposer: EmailComposer
  ) {
    this.storage.create();
    this.loadStorage();

  }

  async loadStorage() {
    this.registres = (await this.storage.get('registres')) || [];
  }

  async guardarRegistre(format: string, text: string) {
    await this.loadStorage();
    const nouRegistre = new Registre(format, text);
    this.registres.unshift(nouRegistre);

    this.storage.set('registres', this.registres);
  }

  obrirRegistre(registre: Registre) {
    const http = 'http';
    const geo = 'geo';
    this.navController.navigateForward('tabs/tab2');

    switch (registre.type) {
      case http:
        this.inAppBrowser.create(registre.text, '_system');
        break;
      case geo:
        this.navController.navigateForward(`/tabs/tab2/maps/${registre.text}`);
        break;
    }
  }

  async escriureArxiu(text: string) {
    await this.file.writeExistingFile(this.file.dataDirectory, 'registres.csv', text)
    const arxiu = `${this.file.dataDirectory}registres.csv`

    let email = {
      to: 'victor.masip.7e4@itb.cat',
      attachments: [
        arxiu
      ],
      subject: 'Backup de scans',
      body: 'Aquí tens la còpia dels codis escanejats. <strong>Scan App</strong>.',
      isHtml: true
    }

    this.emailComposer.open(email)
  }

  crearArxiu(text: string) {
    this.file.checkFile(this.file.dataDirectory, 'registres.csv').then(
      exists => {
        return this.escriureArxiu(text)
      }
    ).catch(err => {
      return this.file.createFile(this.file.dataDirectory, 'registres.csv', false)
      .then(creat => this.escriureArxiu(text))
      .catch(err2 => console.log('No s\'ha pogut crear l\'arxiu', err2))
    })
  }

  enviarEmail() {
    const temp = []
    const titols: string = "Tipus, Format, Creat en, Text\n"

    temp.push(titols)
    this.registres.forEach(element => {
      const linea = `${element.type}, ${element.format}, ${element.created}, 
      ${element.text.replace(',', '')}\n`
      temp.push(linea)
    })

    this.crearArxiu(temp.join(''))
  }
}
