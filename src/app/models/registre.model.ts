export class Registre {
  public format: string;
  public text: string;
  public type: string;
  public icon: string;
  public created: Date;

  constructor(format: string, text: string) {
    this.format = format;
    this.text = text;
    this.created = new Date();

    this.especificaTipus();

  }

  private especificaTipus() {
    const http = 'http';
    const geo = 'geo:';

    const textStart = this.text.substr(0, 4);

    switch (textStart) {
      case http:
        this.type = http;
        this.icon = 'globe';
        break;
      case geo:
        this.type = geo.substr(0, 3);
        this.icon = 'pin';
        break;
      default:
        this.type = 'Unknown';
        this.icon = 'create';
        break;
    }

  }
}
